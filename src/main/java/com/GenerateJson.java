package com;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class GenerateJson {
    public static void main(String args[]) {
        new GenerateJson().generateJson();
    }

    public void generateJson() {
        //read the csv file
        //read the column headers and create corresponding JsonNode
        //Iterate thru the rows and create json array
        //parsing a CSV file into Scanner class constructor
        try {
            Workbook workbook = WorkbookFactory.create(new File("/home/my_data_file.xlsx"));
            Sheet sheet = workbook.getSheetAt(0);
            DataFormatter dataFormatter = new DataFormatter();
            String[] jsonFields = new String[22];
            ArrayNode parentArray = new ObjectMapper().createArrayNode();
            sheet.forEach(row -> {
                //this is the header row
                if (row.getRowNum() == 0) {
                    row.forEach(cell -> {
                        String cellValue = dataFormatter.formatCellValue(cell);
                        jsonFields[cell.getColumnIndex()] = cellValue.replace(' ','_');
                    });
                } else {
                    //process the data
                    JsonNode jsonNode = new ObjectMapper().createObjectNode();
                    row.forEach(cell -> {
                        String cellValue = dataFormatter.formatCellValue(cell);
                        ((ObjectNode) jsonNode).put(jsonFields[cell.getColumnIndex()], cellValue);
                    });
                    parentArray.add(jsonNode);
                }
            });
            try (PrintWriter p = new PrintWriter(new FileOutputStream("output.json"))) {
                p.println(parentArray.toPrettyString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
