# excel-to-json

Java util to convert simple data in excel sheet to json.

Just run the main method to execute, either in an IDE or execute the jar from commands.

Edit the java file to mention absolute path of your excel sheet.

An output.json file is created in the project folder with converted json data.

Any spaces in the column names in the spreadsheet are replaced with underscores in the json data file.

##Assumption
Column names in the spread sheet are in the first row.
Everytime the program is run, the output.json file is overwritten.